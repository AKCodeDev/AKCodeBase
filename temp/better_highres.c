num[i] = tmp % MOD;
		tmp /= MOD;
	}
	if(tmp)
		a->num[a->len++] = tmp;
	__shrink(a);
}

void __div(HP* a, HP* b, HP *res, HP* rem) {
	if(a->len < b->len) {
		*res = ZERO, *rem = *a;
		return;
	}
	I shift;
	static HP tmp;
	for(shift = a->len - b->len; shift >= 0; shift --) {
		I l = 0, r = MOD;
		while(l <= r) {
			I mid = (l+r)/2;
			tmp = *b, __mulI(&tmp, mid);
			if(__comp(&tmp, rem, shift) == -1)
				l = mid+1;
			else r = mid-1;
		}
		a->num[shift] = r;
		tmp = *b, __mulI(b, r);
		//__sub(rem, &tmp, shift);
	}
	res->neg = a->neg ^ b->neg;
	__shrink(res);
}

void __divI(HP* a, I b) {
	if(a < 0)
		b = -b, a->neg ^= 1;
	HP_t tmp = 0;
	I i;
	for(i = 0; i < a->len; i ++) {
		tmp = tmp * MOD + a->num[i];
		a->num[i] = tmp / b;
		tmp %= b;
	}
	__shrink(a);
}

HP getHP() {
	static HP ret;
	ret = ZERO;
	while(!isdigit(*_in))
		if(getC() == '-')
			ret.neg = 1;
	while(isdigit(*_in)) {
		I i;
		HP_t cur = 0;
		for(i = 0; i < EXP && isdigit(*_in); i ++)
			cur = cur * 10 + getC() - '0';
		ret.num[ret.len++] = cur;
	}
	__shrink(&ret);
	return ret;
}

void putHP(HP num) {
	if(num.len) {
		if(num.neg)
			putC('-');
		while(num.len)
			putI(num.num[--num.len]);
	}
	else
		putC('0');
}

I greater(HP a, HP b) {
	if(a.neg != b.neg)
		return a.neg < b.neg;
	else if(a.neg == 1)
		return __comp(&a, &b, 0) == -1;
	else if(a.neg == -1)
		return __comp(&a, &b, 0) == 1;
	return 0;
}

I less(HP a, HP b) {
	if(a.neg != b.neg)
		return a.neg > b.neg;
	else if(a.neg == 1)
		return __comp(&a, &b, 0) == 1;
	else if(a.neg == -1)
		return __comp(&a, &b, 0) == -1;
	return 0;
}

I equal(HP a, HP b) {
	return __comp(&a, &b, 0) == 0;
}

HP add(HP a, HP b) {
	if(a.neg == 0 && b.neg == 0)
		__add(&a, &b);
	else if(a.neg == 0 && b.neg == 1)
		__sub(&a, &b);
	else if(a.neg == 1 && b.neg == 0)
		__sub(&b, &a), a = b;
	else
		__add(&a, &b), a.neg = 1;
	return a;
}

HP sub(HP a, HP b) {
	if(a.neg == 0 && b.neg == 0)
		__sub(&a, &b);
	else if(a.neg == 0 && b.neg == 1)
		__add(&a, &b);
	else if(a.neg == 1 && b.neg == 0)
		__add(&a, &b), a.neg = 1;
	else
		__sub(&b, &a), a = b;
	return a;
}

HP mul(HP a, HP b) {
	static HP ret;
	ret=ZERO;
	__mul(&a, &b, &ret);
	return ret;
}

HP div(HP a, HP b) {
	static HP ret;
	ret=ZERO;
	__div(&a, &b, &ret, NULL);
	return a;
}

HP mod(HP a, HP b) {
	static HP tmp, ret;
	tmp=ZERO, ret=ZERO;
	__div(&a, &b, &tmp, &ret);
	return ret;
}

void solve() {
	HP a=getHP(), b=getHP();
	putHP(add(a, b)),putC('\n');
	putHP(sub(a, b)),putC('\n');
	putHP(mul(a, b)),putC('\n');
	putHP(div(a, b)),putC('\n');
	putHP(mod(a, b));
}

int main() {
	//fread(_inbuf, sizeof(C), sizeof(_inbuf), stdin);
	gets(_inbuf);
	solve();
	fwrite(_outbuf, sizeof(C), _out-_outbuf, stdout);
	return 0;
}
