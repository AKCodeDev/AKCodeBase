#include <ctype.h>
#include <stdio.h>
#include <string.h>

typedef int I;
typedef char C;
typedef double F;

C inbuf[1000001], outbuf[1000001];
C *in = inbuf, *out = outbuf;

I getI() {
	I ret = 0, fac = 1;
	while(!isdigit(*in))
		if(*in++ == '-')
			fac = -1;
	while(isdigit(*in))
		ret = ret*10 + *in++ - '0';
	return ret*fac;
}
void putI(I n) {
	C buf[50], *p = buf;
	if(n < 0)
		*out++ = '-', n = -n;
	do
		*++p = n%10 + '0', n /= 10;
	while(n);
	while(p != buf)
		*out++ = *p--;
}

#define LEN 20001
#define SYS 10
typedef struct {
	C str[LEN];
	I len, neg;
}HD;

HD getHD();
void putHD(HD);
HD ItoHD(I);
I big(HD,HD);
I small(HD,HD);
I equal(HD,HD);
HD add(HD,HD);
HD sub(HD,HD);
HD mul(HD,HD);
HD div(HD,HD);

HD getHD() {
	HD ret=(HD){"",0,0};
    char buf[LEN], *p=buf;
	while(!isdigit(*in))
		if(*in++ == '-')
			ret.neg = 1;
	while(isdigit(*in))
		*++p = *in++ - '0';
    while(p!=buf)
        ret.str[ret.len++] = *p--;
	return ret;
}

void putHD(HD a) {
	if(a.neg)
		*out++ = '-';
	while(a.len>0)
		*out++ = a.str[--a.len]>=10?a.str[a.len]+'A'-10:a.str[a.len]+'0';
}

HD ItoHD(I a) {
	HD ret=(HD){"",0,0};
	if(a < 0)
		ret.neg = 1, a = -a;
	while(a)
		ret.str[ret.len++] = a % SYS, a /= SYS;
	return ret;
}

I big(HD a, HD b) {
	if(a.neg != b.neg)
		return a.neg < b.neg;
	if(a.neg == 1 && b.neg == 1)
		return big(b, a);
	if(a.len != b.len)
		return a.len > b.len;
	C *i = a.str+a.len-1;
	C *j = b.str+b.len-1;
	while(*i == *j && i > a.str && j > b.str)
		i--, j--;
	return *i > *j;
}

I small(HD a, HD b) {
	return big(b, a);
}

I equal(HD a, HD b) {
	if(a.neg != b.neg || a.len != b.len)
        return 0;
    C *i = a.str, *j = b.str;
    while(i < a.str+a.len && j < b.str+b.len) {
        if(*i != *j)
            return 0;
        i ++, j ++;
    }
    return 1;
}

HD add(HD a, HD b) {
	HD ret=(HD){"",0,0};
	I i;
	if(a.neg != b.neg) {
		I bneg = b.neg;
		a.neg = 0;
		b.neg = 0;
		I abig = big(a, b);
		ret=abig?sub(a, b):sub(b, a);
		ret.neg=abig^bneg?1:0;
		return ret;
	}
	for(i = 0; i < a.len || i < b.len; i ++) {
		ret.str[i] += a.str[i] + b.str[i];
		if(i+1<LEN)
			ret.str[i+1] += ret.str[i] >= SYS;
		ret.str[i] %= SYS;
	}
	ret.neg = a.neg&b.neg;
	for(ret.len = i+2; ret.len > 1 && ret.str[ret.len-1] == 0; ret.len --);
	return ret;
}

HD sub(HD a, HD b) {
	
	HD ret=(HD){"",0,0};
	I i;
	if(a.neg != b.neg) {
		I aneg = a.neg;
		a.neg = 0;
		b.neg = 0;
		ret=add(a, b);
		ret.neg = aneg;
		return ret;
	}
	else if(a.neg == 1 && b.neg == 1) {
		a.neg = 0;
		b.neg = 0;
		ret=sub(a, b);
		ret.neg ^= 1;
		return ret;
	}
	else if(big(b, a)) {
		ret=sub(b, a);
		ret.neg = 1;
		return ret;
	}
	for(i = 0; i < a.len || i < b.len; i ++) {
		ret.str[i] += a.str[i] - b.str[i];
		if(i+1<LEN)
			ret.str[i+1] -= ret.str[i] < 0;
		ret.str[i] = (ret.str[i]+SYS) % SYS;
	}
	for(ret.len = i+1; ret.len > 1 && ret.str[ret.len-1] == 0; ret.len --);
	return ret;
}

HD mul(HD a, HD b) {
	HD ret=(HD){"",0,0};
	I i, j;
	ret.neg = a.neg ^ b.neg;
	for(i = 0; i < a.len; i ++)
		for(j = 0; j < b.len; j ++) {
			if(i+j<LEN)
				ret.str[i+j] += a.str[i]*b.str[j];
			if(i+j+1<LEN)
				ret.str[i+j+1] += ret.str[i+j]/SYS;
			if(i+j<LEN)
				ret.str[i+j] %= SYS;
		}
    for(ret.len = i+j+2; ret.len > 1 && ret.str[ret.len-1] == 0; ret.len --);
	return ret;
}

HD div(HD a, HD b) {
	HD ret=(HD){"",0,0};
	HD c, tmp, pow;
	I neg = a.neg ^ b.neg;
	a.neg = 0;
	b.neg = 0;
    I lena = a.len, lenb = b.len, i;
    c = b, tmp = mul(c,ItoHD(SYS)), pow = ItoHD(1);
    while(!big(tmp, a)) {
        c = tmp;
        pow = mul(pow, ItoHD(SYS));
        tmp = mul(c,ItoHD(SYS));
    }
	while(big(a, b) || equal(a, b)) {
		tmp = sub(a, c);
		while(!big(ItoHD(0), tmp)) {
			a = tmp;
			ret = add(ret, pow);
            tmp = sub(tmp, c);
		}
        for(i = pow.len - 1; i < c.len; i ++)
            c.str[i-1]=c.str[i];
        c.str[i-1] = 0;
        c.len--;
        if(pow.len > 1)
            pow.str[pow.len-2]=1;
        pow.str[pow.len-1]=0;
        pow.len--;
	}
    for(ret.len = lena + 2; ret.len > 1 && ret.str[ret.len-1] == 0; ret.len --);
	ret.neg = neg;
	return ret;
}

void solve() {
	HD a=getHD(), b=getHD();
	putHD(div(a, b));
}

int main() {
	fread(inbuf, sizeof(C), sizeof(inbuf), stdin);
	solve();
	fwrite(outbuf, sizeof(C), out-outbuf, stdout);
	return 0;
}
