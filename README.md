# AKCodeBase
### 目录
#### exercises（练习题）
信息竞赛练习题，附带题解
#### Libraries（库）
可直接使用的代码库
#### Temp（临时文件）
存储临时文件
#### Tools（工具）
实用脚本工具
#### Toys（玩具）
游戏或趣味代码
