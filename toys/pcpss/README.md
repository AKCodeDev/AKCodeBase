# PCPSS
特别的石头剪刀布战斗游戏 

## 试玩
#### Unix
使用 [GCC](http://gcc.gnu.org/index.html) 编译
#### Windows
使用 [dev-c++](https://sourceforge.net/projects/orwelldevcpp/) 编译

## 小贴士
No.1: 不要只拷贝 pcpss.c。

No.2: 本游戏完全由 akcodedev 制作。

No.3: 本游戏支持 Linux, OSX 和 Windows。

## 已知漏洞
No.1: 在停顿中输入文字将视作命令。
