# PCPSS
A paper-scissors-stone featured turn-based battle game

## How to play it
#### Unix
Compile with [GCC](http://gcc.gnu.org/index.html)
#### Windows
Compile with [dev-c++](https://sourceforge.net/projects/orwelldevcpp/) (or others)

## Tips
No.1: You should copy the whole folder instead of the main code.

No.2: This game is COMPLETELY made by akcodedev.

No.3: This game supports not only Linux but also Windows and OSX.

## Known issue
No.1: Command will be input if players input something during system sleep.
