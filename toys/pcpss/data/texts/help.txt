!y
      +-------------------------------------------+
      |    PCPSS gameplay                         |
      | Though the game is a kind of PSS game, it |
      | is more similar to battle game. So it'll  |
      | have a more complex gameplay.             |
      | Each game has several turns, and each tu- |
      | rn you'll play a PSS with AI.             |
      | There are two states in the game: active  |
      | state and passive state. Active state the |
      | AI's hp will be influenced, and Passive   |
      | state yours will be influenced.           |
      | If someone win, he will gain hp or the    |
      | others will lose hp. It depends on state. |
      | If tie, the state will change.            |
      +-------------------------------------------+
      |    PCPSS gamemode                         |
      | PCPSS has 3 gamemodes, they are adventure |
      | mode, battle mode and endless mode.       |
      | In adventure mode, you will beat PC with  |
      | much less power and it will be stronger   |
      | if you beat one. The more you beat, The   |
      | more score you will get.                  |
      | In battle mode, you will fight with a PC  |
      | with same power.                          |
      | In endless mode, PC has endless hp. You   |
      | will get score if PC loses hp.            |
      +-------------------------------------------+
      |    PCPSS help                             |
      | The PCPSS is a paper-scissors-stone game. |
      | Its full name is PC Paper Scissors Stone. |
      |    PCPSS command                          |
      | When you are in game, use these commands: |
      |    '0' for stone                          |
      |    '2' for scissors                       |
      |    '5' for paper                          |
      |    'q' for end the game immediately       |
      |    'p' for pause and quit                 |
      |    'c' for show the credit                |
      |    'i' for show the info                  |
      | These are for the menu:                   |
      |    'a' for start adventure gamemode       |
      |    'b' for start battle gamemode          |
      |    'e' for start endless gamemode         |
      |    'q' for exit the game                  |
      |    'p' for start a paused game            |
      |    'c' for show the credit                |
      |    'i' for show the info                  |
      | You can use 'h' for showing the message.  |
      +-------------------------------------------+!w
