#include <stdio.h>
#define M 100001
int stk[M]={1}, n;
void dfs(int dep, int rem) {
	int i;
	if(!rem) {
		printf("%d=", n);
		for(i = 1; i < dep; i ++)
			printf("%d+",stk[i]);
		printf("%d\n",stk[dep]);
	}
	for(i = stk[dep]; i <= rem; i ++) {
		if(!dep&&i==rem)continue;
		stk[dep+1]=i;
		dfs(dep+1, rem-i);
	}
}
int main() {
	scanf("%d", &n);
	dfs(0,n);
	return 0;
}