#include <stdio.h>
#define M 1002

typedef struct {
	int x,y,g,h;
}node;
#define SWAP(a,b) ({node tmp=a;a=b;b=tmp;})
node pq[M*M];
int len;
void push(node a) {
	pq[++len]=a;
	int i;
	for(i = len; i > 1; i >>= 1) {
		if(pq[i>>1].g+pq[i>>1].h<pq[i].g+pq[i].h)	
			SWAP(pq[i],pq[i>>1]);
		else break;
	}
}
void pop() {
	SWAP(pq[1], pq[len]);
	len--;
	int i, cur;
	for(i = 1; i << 1 <= len; i=cur) {
		cur=pq[(i<<1)+1].g+pq[(i<<1)+1].h<pq[(i<<1)].g+pq[(i<<1)].h&&i<<1<len?(i<<1)+1:i<<1;
		if(pq[cur].g+pq[cur].h>pq[i].g+pq[i].h)
			SWAP(pq[cur], pq[i]);
		else break;
	}
}
char map[M][M],vis[M][M];
#define D 4
int dx[D]={1,-1,0,0},dy[D]={0,0,-1,1};
#define ABS(a) (a>0?a:-a)
int main() {
	int n,i,a,b,c,d;
	scanf("%d ", &n);
	for(i = 1; i <= n; i ++)
		scanf("%s",map[i]+1);
	for(i = 1; i <= n; i ++)
		map[0][i]=map[n+1][i]=map[i][0]=map[i][n+1]='1';
	scanf("%d%d%d%d", &a,&b,&c,&d);
	push((node){a,b,0,ABS(c-a)+ABS(d-b)});
	while(len) {
		node u=pq[1];
		pop();
		if(u.x==c&&u.y==d){
			printf("%d",u.g);
			return 0;
		}
		for(i = 0; i < D; i ++) {
			int tx=u.x+dx[i],ty=u.y+dy[i];
			if(map[tx][ty]=='0'&&vis[tx][ty]==0) {
				vis[tx][ty]=1;
				push((node){tx,ty,u.g+1,ABS(c-tx)+ABS(d-ty)});
			}
		}
	}
	printf("Impossible!");
	return 0;
}