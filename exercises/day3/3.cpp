#include <stdio.h>
#include <string.h>
#define M 121
#define N 21
int a[N], dp[M*N];
int main() {
	int t;
	scanf("%d", &t);
	while(t--) {
		int n,i,j,sum=0,min=M*N,cur;
		scanf("%d", &n);
		for(i = 1; i <= n; i ++) {
			scanf("%d", &a[i]);
			sum+=a[i];
		}
		dp[0]=1;
		for(i = 1; i <= n; i ++) {
			for(j = 0; j <= sum; j ++) {
				if(dp[j])
					dp[j + a[i]] = 1;
			}
		}
		for(j = 0; j <= sum; j ++)
			if(dp[j])
				cur=sum-j>j?sum-j-j:j+j-sum,min=min<cur?min:cur;
		printf("%d\n", min);
	}
	return 0;
}