#include <stdio.h>
#define M 21
int stk[M], a, b;
void dfs(int dep){
	int i;
	if(dep == b) {
		for(i = 1; i <= b; i ++)
			printf("%d ", stk[i]);
		printf("\n");
		return;
	}
	for(i = stk[dep] + 1; i <= a; i ++) {
		stk[dep+1]=i;
		dfs(dep+1);
	}
}
int main() {
	scanf("%d%d", &a, &b);
	dfs(0);
	return 0;
}