#include <stdio.h>
int matrix[2049][2049];
void dfs(int dep, int num, int x, int y) {
	if(dep == 0) {
		matrix[x][y] = num;
		return;
	}
	dfs(dep-1, num, x, y);
	dfs(dep-1, num + (1<<(dep-1)), x + (1<<(dep-1)), y);
	dfs(dep-1, num + (1<<(dep-1)), x, y + (1<<(dep-1)));
	dfs(dep-1, num, x + (1<<(dep-1)), y + (1<<(dep-1)));
}
int main() {
	int i, j, dep;
	scanf("%d", &dep);
	dfs(dep, 1, 1, 1);
	for(i = 1; i <= (1<<dep); i ++) {
		for(j = 1; j <= (1<<dep); j ++)
			printf("%d ", matrix[i][j]);
		printf("\n");
	}
	return 0;
}