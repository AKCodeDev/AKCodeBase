#include <stdio.h>
#define EPS 1e-5
double a,b,c,d;
int val(double x) {
	double res = a*x*x*x+b*x*x+c*x+d;
	if(res < EPS && res > -EPS)
		return 0;
	else if(res >= EPS)
		return 1;
	else
		return -1;
}
int main() {
	int i;
	scanf("%lf%lf%lf%lf", &a, &b, &c, &d);
	for(i = -100; i < 100; i ++) {
		double l = (double)i + EPS, r = (double)(i+1);
		int e = val(l), f = val(r);
		if(e * f > 0)
			continue;
		while(r - l > EPS) {
			double m = (l+r) / 2.0;
			if(val(m) * e > 0)
				l = m;
			else
				r = m;
		}
		printf("%.2lf\n", l);
	}
	return 0;
}