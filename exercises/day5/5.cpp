#include <stdio.h>
#include <string.h>
#define M 101
int num[M], dp[M][M], val[M][M];
int min(int a, int b) {
	return a<b?a:b;
}
int abs(int a) {
	return a>0?a:-a;
}
int main() {
	memset(dp,127,sizeof(dp));
	int n,m,i,j,k;
	scanf("%d%d", &n, &m);
	for(i = 1; i <= n; i ++)
		scanf("%d",&num[i]);
	for(i = 1; i <= n; i ++) {
		for(j = 1; i + j - 1 <= n; j ++) {
			int mid=(2*i+j)/2;
			for(k = i; k <= i + j - 1; k ++) {
				val[i][i+j-1] += abs(num[k]-num[mid]);
			}
		}
	}
	for(i = 1; i <= n; i ++)
		dp[i][1]=val[1][i];
	for(i = 2; i <= m; i++) {
		for(j = i; j <= n; j ++) {
			for(k = 1; k < j; k ++) {
				dp[j][i]=min(dp[j][i],dp[k][i-1]+val[k+1][j]);
			}
		}
	}
	printf("%d",dp[n][m]);
	return 0;
}
