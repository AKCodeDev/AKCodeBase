#include <stdio.h>
#define M 101
int num[M], dp[M][M];
int max(int a, int b) {
	return a<b?a:b;
}
int main() {
	int n,i,j,k;
	scanf("%d", &n);
	for(i=1;i<=n;i++) {
		scanf("%d",&num[i]);
		dp[i][i]=1;
	}
	for(i =3;i<=n;i++)
		for(j=1;i+j-1<=n;j++) {
			int mx=100000000;
			for(k=j+1;k<=i+j-2;k++)
				mx=max(mx,dp[j][k]+dp[k][i+j-1]+num[j]*num[i+j-1]*num[k]);
			dp[j][i+j-1]=mx;
		}
	printf("%d",dp[1][n]);
	return 0;
}
