#include <stdio.h>
#define M 51
int dp[M*2][M][M], map[M][M];
int mx(int a, int b){return a>b?a:b;
}
int max(int a, int b, int c, int d) {
	return mx(mx(a,b),mx(c,d));
}
int main() {
	int m,n,i,j,k;
	scanf("%d%d", &m, &n);
	for(i = 1; i <= m; i ++)
		for(j = 1; j <= n; j ++) {
			scanf("%d",&map[i][j]);
		}
	for(i = 1; i <= m+n; i ++) {
		for(j = 1; j <= m; j ++)
			for(k = 1; k <= m; k ++) {
				dp[i][j][k]=max(dp[i-1][j][k],dp[i-1][j-1][k],dp[i-1][j][k-1],dp[i-1][j-1][k-1])+map[j][i-j]+(j!=k)*map[k][i-k];
			}
	}
	printf("%d",dp[m+n][m][m]);
	return 0;
}
