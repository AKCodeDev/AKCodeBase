#include <stdio.h>
#define M 101
int dp[M][M];
int max(int a, int b) {
	return a>b?a:b;
}
int main() {
	int n,m,i,j,k;
	scanf("%d%d", &n, &m);
	for(i = 1; i <= n; i ++) {
		scanf("%d", &dp[i][0]);
		dp[i][0]+=dp[i-1][0];
	}
	for(i = 1; i <= m; i ++) {
		for(j = 1; j <= n; j ++) {
			for(k = i; k < j; k ++) {
				dp[j][i]=max(dp[j][i],dp[k][i-1]*(dp[j][0]-dp[k][0]));
			}
		}
	}
	printf("%d",dp[n][m]);
	return 0;
}
