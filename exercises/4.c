#include <stdio.h>
char str[4][11] = {"ooo*o**--*", "o--*o**oo*", "o*o*o*--o*", "--o*o*o*o*"};
int main() {
	int i,j, n;
	scanf("%d", &n);
	for(i = 1; i <= 2*n - 6; i ++) {
		printf("step%2d:", i-1);
		for(j = 1; j <= n - i/2; j ++) {
			printf("o");
		}
		if(i%2==0) {
			printf("--");
		}
		for(j = 1; j <= n - i/2; j ++) {
			printf("*");
		}
		if(i%2==1) {
			printf("--");
		}
		for(j = 1; j <= i/2; j ++) {
			printf("o*");
		}
		printf("\n");
	}
	for(i; i <= 2*n-2; i ++) {
		printf("step%2d:", i-1);
		printf("%s",str[i-(2*n-5)]);
		for(j = 1; j <= n-4; j ++) {
			printf("o*");
		}
		printf("\n");
	}
	return 0;
}