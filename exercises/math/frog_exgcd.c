#include <stdio.h>
typedef long long I;
I ans, x1, y1;
I exgcd(I a, I b) {
	if(b == 0) {
		x1 = 1;
		y1 = 0;
		return a;
	}
	ans = exgcd(b, a%b);
	I tmp = x1;
	x1 = y1;
	y1 = tmp - a/b*y1;
	return ans;
}
I mod(I x, I m) {
	return (x%m+m)%m;
}
int main() {
	I x, y, m, n, l;
	scanf("%lld%lld%lld%lld%lld", &x, &y, &m, &n, &l);
	I b = n-m, a = x-y;
	if(b < 0)
		b = -b, a = -a;
	exgcd(b, l);
	if(a % ans)
		printf("Impossible");
	else
		printf("%lld", mod(x1*(a/ans), l/ans));
	return 0;
}