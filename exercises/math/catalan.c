#include <stdio.h>

#define LEN 1000

typedef struct{
	int len;
	char val[LEN+5];
}HD;

HD add(HD a, HD b) {
	int i;
	HD ret  = (HD){0,""};
	for(i = 1; i <= a.len || i <= b.len; i ++) {
		ret.val[i] += a.val[i]+ b.val[i];
		ret.val[i+1] = ret.val[i] / 10;
		ret.val[i] %= 10;
	}
	ret.len = i;
	if(!ret.val[i])
		ret.len --;
	return ret;
}

void print(HD a) {
	while(a.len--)
		putchar(a.val[a.len+1] + '0');
}

#define N 1000

HD catalan[N+5];

int main() {
	int i, j, n;
	scanf("%d", &n);
	for(i = 1; i <= n; i ++) {
		catalan[i].len = 1;
		catalan[i].val[1] = 1;
	}
	for(i = 1; i <= n; i ++) {
		for(j = i+1; j <= n; j ++)
			catalan[j] = add(catalan[j-1], catalan[j]);
	}
	print(catalan[n]);
	return 0;
}