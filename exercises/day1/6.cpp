#include <stdio.h>
int a[21];
void sort(int l, int r, int(*cmp)(int,int)) {
	int m=a[(l+r)/2];
	int i=l,j=r;
	do{
		while(cmp(a[i],m))i++;
		while(cmp(m,a[j]))j--;
		if(i<=j){
			int tmp=a[i];
			a[i]=a[j];
			a[j]=tmp;
			i++,j--;
		}
	}while(i<=j);
	if(j>l)sort(l,j,cmp);
	if(i<r)sort(i,r,cmp);
}
int cmp(int a, int b) {
	int ta=a,tb=b,al=1,bl=1;
	while(ta)al*=10,ta/=10;
	while(tb)bl*=10,tb/=10;
	return a*bl+b>b*al+a;
}
int main() {
	int n,i;
	scanf("%d", &n);
	for(i = 1; i <= n; i ++)
		scanf("%d",&a[i]);
	sort(1,n,cmp);
	for(i = 1; i <= n; i ++)
		printf("%d",a[i]);
	return 0;
}
