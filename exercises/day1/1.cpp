#include <stdio.h>

typedef unsigned long T;

#define M 1000001

void msort(T *arr, T l, T r) {
	if(l==r)return;
	static T tmp[M];
	T mid=(l+r)/2;
	msort(arr,l,mid);
	msort(arr,mid+1,r);
	T i=l,j=mid+1,k=l;
	while(i<=mid&&j<=r)
		tmp[k++]=arr[i]>arr[j]?arr[j++]:arr[i++];
	while(i<=mid)
		tmp[k++]=arr[i++];
	while(j<=r)
		tmp[k++]=arr[j++];
	for(k=l;k<=r;k++)
		arr[k]=tmp[k];
}

T num[M];

int main() {
	int n,i;
	scanf("%lu", &n);
	for(i = 1; i <= n; i ++) 
		scanf("%lu", &num[i]);
	msort(num,1,n);
	for(i = 1; i <= n; i++)
		printf("%lu", num[i]);
	return 0;
}
