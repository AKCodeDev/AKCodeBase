#include <stdio.h>
double a[100001]={1.0};
int main() {
	int i, b, n, mark=0;
	scanf("%d", &n);
	for(i = 1; i <= n; i++)
		scanf("%d", &b), a[i]=(double)b/100.0;
	double cur = 100.0;
	for(i = 1; i < n; i++) {
		if(cur*a[i]/a[i+1]>cur)
			cur=cur*a[i]/a[i+1];
	}
	printf("%.1lf",cur);
	return 0;
}