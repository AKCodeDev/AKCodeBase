#include <stdio.h>
int main() {
	int a=1,b=2,c=3,n;
	scanf("%d", &n);
	while(--n) {
		a=b;
		b=c;
		c=a+b;
	}
	printf("%d", a);
	return 0;
}
