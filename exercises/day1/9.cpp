#include <stdio.h>
#define M 10001
int wi[M], li[M];
void sort(int l, int r) {
	int m=wi[(l+r)>>1];
	int i=l,j=r;
	do {
		while(wi[i]<m)++i;
		while(wi[j]>m)--j;
		if(i<=j) {
			int tmp=wi[i];wi[i]=wi[j];wi[j]=tmp;
			tmp=li[i];li[i]=li[j];li[j]=tmp;
			i++,j--;
		}
	}while(i<=j);
	if(j>l)sort(l,j);
	if(i<r)sort(i,r);
}

int buf[M],len;
int lower_bound(int i) {
	int l=1,r=len;
	while(l<=r) {
		int m=(l+r)>>1;
		if(buf[m]>i)l=m+1;
		else r=m-1;
	}
	return l;
}
int main() {
	int n;
	scanf("%d", &n);
	int i;
	for(i=1;i<=n;i++) {
		scanf("%d%d", &li[i], &wi[i]);
	}
	sort(1,n);
	buf[len]=li[++len];
	for(i=2;i<=n;i++) {
		if(li[i]<buf[len])
			buf[++len]=li[i];
		else
			buf[lower_bound(li[i])]=li[i];
	}
	printf("%d",len);
	return 0;
}
