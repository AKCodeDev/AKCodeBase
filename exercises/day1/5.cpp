#include <stdio.h>
#define M 5001
int time[M],ind[M];
void sort(int l, int r) {
	int m=time[(l+r)/2];
	int i=l,j=r;
	do {
		while(time[i]<m)i++;
		while(time[j]>m)j--;
		if(i<=j){
			int tmp=time[i];time[i]=time[j];time[j]=tmp;
			tmp=ind[i];ind[i]=ind[j];ind[j]=tmp;
			i++,j--;
		}
	}while(i<=j);
	if(j>l)sort(l,j);
	if(i<r)sort(i,r);
}
int main() {
	int n,i;
	double ans=0.0;
	scanf("%d", &n);
	for(i=1;i<=n;i++)scanf("%d",&time[i]),ind[i]=i;
	sort(1,n);
	for(i=1;i<=n;i++)printf("%d ",ind[i]);
	for(i=1;i<=n;i++)ans+=(double)((n-i)*time[i]);
	ans/=(double)n;
	printf("\n%.2lf",ans);
	return 0;
}
