#include <stdio.h>
#define M 22
int dp[M][M]={1};
int dx[9]={0,1,2,-1,-2,-1,-2,1,2};
int dy[9]={0,2,1,-2,-1,2,1,-2,-1};
int main() {
	int n,m,a,b,i,j,k;
	scanf("%d%d%d%d", &n, &m, &a, &b);
	a++,b++;
	for(i=1;i<=n+1;i++)
		for(j=1;j<=m+1;j++) {
			dp[i][j]=dp[i-1][j]+dp[i][j-1];
			for(k=0;k<=8;k++)
				if(i==a+dx[k]||i==b+dy[k]) {
					dp[i][j]=0;
					break;
				}
		}
	printf("%d",dp[n+1][m+1]);
	return 0;
}
