#include <stdio.h>

typedef unsigned long T;

#define M 1000001

T msort(T *arr, T l, T r) {
	if(l==r)return 0;
	static T tmp[M];
	T mid=(l+r)/2,ans=0;
	ans+=msort(arr,l,mid);
	ans+=msort(arr,mid+1,r);
	T i=l,j=mid+1,k=l;
	while(i<=mid&&j<=r) {
		if(arr[i]>arr[j]) {
			tmp[k++]=arr[j++];
			ans+=mid-i+1;
		}
		else tmp[k++]=arr[i++];
	}
	while(i<=mid)
		tmp[k++]=arr[i++];
	while(j<=r)
		tmp[k++]=arr[j++];
	for(k=l;k<=r;k++)
		arr[k]=tmp[k];
	return ans;
}

T num[M];

int main() {
	int n,i;
	scanf("%lu", &n);
	for(i = 1; i <= n; i ++) 
		scanf("%lu", &num[i]);
	printf("%lu",msort(num,1,n));
	
	return 0;
}
