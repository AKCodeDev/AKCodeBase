#include <stdio.h>
#define M 100001
int a[M], n;
void sort(int l, int r) {
	int m = a[(l+r)/2];
	int i = l, j = r;
	do{
		while(a[i] > m)i++;
		while(a[j] < m)j--;
		if(i <= j) {
			int tmp = a[i];
			a[i] = a[j];
			a[j] = tmp;
			i++;
			j--;
		}	
	}while(i <= j);
	if(j > l)sort(l, j);
	if(i < r)sort(i, r); 
}
int main() {
	int i, max;
	scanf("%d%d", &max, &n);
	for(i = 1; i <= n; i ++)
		scanf("%d", &a[i]);
	sort(1, n);
	int ans = 0;
	for(i = 1; i <= n; i++) {
		if(a[i] + a[n] <= max)
			n --;
		ans++;
	}
	printf("%d", ans);
	return 0;
}
