#include <stdio.h>
#define M 30001
#define N 21
int dp[M*N]={1};
int val[4]={0,150,200,350};
int main() {
	int m,n,i,j,a;
	scanf("%d", &m);
	for(i=1;i<=3;i++) {
		a=val[i];
		for(j=0;j<=m;j++)
			if(dp[j])
				dp[j+a]=1;
	}
	for(i=m;i>=0;i--)
		if(dp[i])
			break;
	printf("%d",m-i);
	return 0;
}
