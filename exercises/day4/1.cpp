#include <stdio.h>
#define M 30001
#define N 21
int dp[M*N]={1};
int main() {
	int m,n,i,j,a;
	scanf("%d%d", &m, &n);
	for(i=1;i<=n;i++) {
		scanf("%d", &a);
		for(j=m;j>=0;j--)
			if(dp[j])
				dp[j+a]=1;
	}
	for(i=m;i>=0;i--)
		if(dp[i])
			break;
	printf("%d",m-i);
	return 0;
}
