#include <stdio.h>
#define M 100001
#define N 101
int dp[M]={1},a[N],c[N];
int main() {
	int n,m,i,j,k;
	scanf("%d%d", &n, &m);
	for(i = 1; i <= n; i ++)
		scanf("%d", &a[i]);
	for(i = 1; i <= n; i ++)
		scanf("%d", &c[i]);
	for(i = 1; i <= n; i ++)
		for(j = m; j >= 0; j --)
			if(dp[j])
				for(k = 1; k <= c[i]; k ++)
					dp[j+k*a[i]]=1;
	int ans=0;
	for(i = 1; i <= m;i++)
		ans+= dp[i],printf("%d ",dp[i]);
	printf("%d", ans);
	return 0;
}
