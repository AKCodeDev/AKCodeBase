#include <stdio.h>
#include <string.h>
#define M 1001
char str1[M],str2[M];
int len1, len2, dp[M][M];
#define MAX(a,b) ((a)>(b)?(a):(b))
int main() {
	int i,j;
	scanf("%s%s",str1+1,str2+1);
	len1=strlen(str1+1), len2=strlen(str2+1);
	for(i=1;i<=len1;i++)
		for(j=1;j<=len2;j++){
			if(str1[i]==str2[j])
				dp[i][j]=dp[i-1][j-1]+1;
			else
				dp[i][j]=MAX(dp[i-1][j],dp[i][j-1]);
		}
	printf("%d",dp[len1][len2]);
	return 0;
}
