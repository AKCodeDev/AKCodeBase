#include <stdio.h>
#include <string.h>
#define M 2000001
char str[M];
int len;
char stk[M];
int top;
int main() {
	int t,i,flag;
	scanf("%d", &t);
	while(t--) {
		memset(str,0,sizeof(str));
		scanf("%s",str);
		len=strlen(str);
		top=0,flag=1;
		for(i = 0; i < len; i ++) {
			if(str[i]=='('||str[i]=='['||str[i]=='{'||str[i]=='<')
				stk[++top]=str[i];
			else {
				if(top==0||(stk[top]!=str[i]-1&&stk[top]!=str[i]-2)){
					flag=0;
					break;
				}
				else
					top--;
			}
		}
		puts(flag?"TRUE\n":"FALSE\n");
	}
	return 0;
}
