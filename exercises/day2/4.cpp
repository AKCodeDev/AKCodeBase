#include <stdio.h>
#define M 1000001
int minq[M],ind1[M],f1=1,b1=0;
int maxq[M],ind2[M],f2=1,b2=0;
void pushmin(int num, int ind) {
	minq[++b1]=num;
	ind1[b1]=ind;
	while(minq[b1-1]>minq[b1]&&b1>f1) {
		minq[b1-1]=minq[b1];
		ind1[b1-1]=ind1[b1];
		b1--;
	}
}
void pushmax(int num, int ind) {
	maxq[++b2]=num;
	ind2[b2]=ind;
	while(maxq[b2-1]<maxq[b2]&&b2>f2) {
		maxq[b2-1]=maxq[b2];
		ind2[b2-1]=ind2[b2];
		b2--;
	}
}
int min[M],len,max[M];
int main() {
	int n,w,i,j,a;
	scanf("%d%d", &n, &w);
	for(i=1;i<=n;i++) {
		
		scanf("%d", &a);
		pushmin(a,i);
		pushmax(a,i);
		if(i>=w) {
			min[++len]=minq[f1];
			max[len]=maxq[f2];
			if(i-w>=ind1[f1]-1)f1++;
			if(i-w>=ind2[f2]-1)f2++;
		}
	}
	for(i=1;i<=len;i++)
		printf("%d ",min[i]);
	printf("\n");
	for(i=1;i<=len;i++)
		printf("%d ",max[i]);
	return 0;
}
