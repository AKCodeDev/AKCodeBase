#include <stdio.h>
#define M 1001
int stk[M], len;
int main() {
	int n, cur=1, a;
	scanf("%d", &n);
	while(n--) {
		scanf("%d", &a);
		stk[++len]=a;
		while(stk[len]==cur)cur++,len--;
	}
	puts(len?"NO":"YES");
}
