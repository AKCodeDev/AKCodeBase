#include <stdio.h>
int main() {
	long long b,p,k;
	scanf("%lld%lld%lld", &b, &p, &k);
	long long ans = 1, fac = b, pow = p;
	while(p) {
		if(p & 1)
			ans = (ans * fac) % k;
		fac = (fac * fac) % k;
		p >>= 1;
	}
	printf("%lld^%lld mod %lld=%lld", b,pow,k,ans);
	return 0;
}